'''Grade School
Write a small archiving program that stores students' names along with the grade that they are in.

In the end, you should be able to:

Add a student's name to the roster for a grade
"Add Jim to grade 2."
"OK."
Get a list of all students enrolled in a grade
"Which students are in grade 2?"
"We've only got Jim just now."
Get a sorted list of all students in all grades. Grades should sort as 1, 2, 3, etc., and students within a grade should be sorted alphabetically by name.
"Who all is enrolled in school right now?"
"Grade 1: Anna, Barb, and Charlie. Grade 2: Alex, Peter, and Zoe. Grade 3..."
Note that all our students only have one name. (It's a small town, what do you want?)

'''

class School():


    def __init__(self, name):
        self.name = name
        self.roster = {}    # ( grade: [students])

    def grade(self, grade_level):
        if grade_level in self.roster:
            students = self.roster[grade_level]
        else:
            students = []

        return students

    def add(self, student, grade_level):
        if grade_level in self.roster:
            self.roster[grade_level].append(student)
        else:
            self.roster[grade_level] = [student]
        return 

    def sort(self):
        keys = self.roster.keys()
        keys.sort()
        everyone = []

        for key in keys:

            everyone.append( (key, tuple(self.roster[key])) )

        return everyone
