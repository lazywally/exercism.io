def hello(name="World"):
    if name == "" or name == None:
        name = "World"
    return ("Hello, " + name + "!")
