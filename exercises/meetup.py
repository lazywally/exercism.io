'''Meetup
Calculate the date of meetups.

Typically meetups happen on the same day of the week.

Examples are

the first Monday
the third Tuesday
the Wednesteenth
the last Thursday
Note that "Monteenth", "Tuesteenth", etc are all made up words. There was a meetup whose members realised that there are exactly 7 days that end in '-teenth'. Therefore, one is guaranteed that each day of the week (Monday, Tuesday, ...) will have exactly one date that is named with '-teenth' in every month.
'''

import datetime
import calendar

def meetup_day(year, month, day_of_week, week_of_month):
    x_teenth = range(13, 20)
    days = {1:"Monday", 2:"Tuesday", 3:"Wednesday", 4:"Thursday", 5:"Friday", 6:"Saturday", 7:"Sunday"}

    if week_of_month == "teenth":
        week = range(13,20)
    elif week_of_month == "1st":
        week = range(1,8)
    elif week_of_month == "2nd":
        week = range(8,15)
    elif week_of_month == "3rd":
        week = range(15,23)
    elif week_of_month == "4th":
        week = range(23,30)
    elif week_of_month == "5th":
        week = range(24, 31)
    elif week_of_month == "last":
        if month == 2:                # Feb is special case
            if calendar.isleap(year): 
                week = range(29, 22, -1)
            else:
                week = range(28, 21, -1)
        else:
            week = range(31, 22, -1)    # count backwards 5th and 4th weeks


    for day in week:
        meeting_date = datetime.date(year, month, day)
        if days[meeting_date.isoweekday()] == day_of_week:
            return meeting_date
        print [year, month, day]
