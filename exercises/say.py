'''Say
Write a program that will take a number from 0 to 999,999,999,999 and spell out that number in English.

Step 1

Handle the basic case of 0 through 99.

If the input to the program is 22, then the output should be 'twenty-two'.

Your program should complain loudly if given a number outside the blessed range.

Some good test cases for this program are:

0
14
50
98
-1
100
Extension

If you're on a Mac, shell out to Mac OS X's say program to talk out loud.

Step 2

Implement breaking a number up into chunks of thousands.

So 1234567890 should yield a list like 1, 234, 567, and 890, while the far simpler 1000 should yield just 1 and 0.

The program must also report any values that are out of range.

Step 3

Now handle inserting the appropriate scale word between those chunks.

So 1234567890 should yield '1 billion 234 million 567 thousand 890'

The program must also report any values that are out of range. It's fine to stop at "trillion".

Step 4

Put it all together to get nothing but plain English.

12345 should give twelve thousand three hundred forty-five.

The program must also report any values that are out of range.

Extensions

Use and (correctly) when spelling out the number in English:

14 becomes "fourteen".
100 becomes "one hundred".
120 becomes "one hundred and twenty".
1002 becomes "one thousand and two".
1323 becomes "one thousand three hundred and twenty-three".
'''

english = { 
            1: 'one',
            2: 'two',
            3: 'three',
            4: 'four',
            5: 'five',
            6: 'six',
            7: 'seven',
            8: 'eight',
            9: 'nine',
            10: 'ten',
            11: 'eleven',
            12: 'twelve',
            13: 'thirteen',
            14: 'fourteen',
            15: 'fifteen',
            16: 'sixteen',
            17: 'seventeen',
            18: 'eighteen',
            19: 'nineteen',
            20: 'twenty',
            30: 'thirty',
            40: 'forty',
            50: 'fifty',
            60: 'sixty',
            70: 'seventy',
            80: 'eighty',
            90: 'ninety'
          }



MIN = 0
MAX = 999999999999    # 999 billion

def say(number):

    if number == 0:
        return "zero"

    if number < MIN or number > MAX:
        raise AttributeError("number out of range")

    plain_english = ""
    keys = english.keys()
    keys.sort()
    keys.reverse()


    billions = number//1e9

    if billions:
        if number % 1e9:
            if number % 1e9 < 100:
                return say(billions) + " billion and " + say(number%1e9) 
            else: 
                return say(billions) + " billion " + say(number%1e9)
        else:
            return say(billions) + " billion" 


    millions = number//1e6

    if millions:
        if number % 1e6:
            if number % 1e6 < 100:
                return say(millions) + " million and " + say(number%1e6) 
            else: 
                return say(millions) + " million " + say(number%1e6) 
        else:
            return say(millions) + " million" 

    thousands = number//1000

    if thousands:
        if number % 1000:
            if number % 1000 < 100:
                return say(thousands) + " thousand and " + say(number%1000) 
            else: 
                return say(thousands) + " thousand " + say(number%1000)
        else:
            return say(thousands) + " thousand" 

    hundreds = number//100

    if hundreds:
        if number % 100:
            if number % 100 < 100:    # always true, exists for consistancy
                return say(hundreds) + " hundred and " + say(number%100) 
            else:    # this should never hit, but exists for consistancy with above code
                return say(hundreds) + " hundred " + say(number%100)
        else:
            return say(hundreds) + " hundred" 
         

    # sub-hundred

    for key in keys:
        if number >= key:
            print key
            number -= key
            if plain_english != "":
                plain_english += "-"
            plain_english = plain_english + english[key]
    return plain_english
    
