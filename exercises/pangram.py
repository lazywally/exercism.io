def is_pangram(sentence):
    sentence = sentence.lower()
    for letter in ["a", "b", "c", "d", "e",
                   "f", "g", "h", "i", "j",
                   "k", "l", "m", "n", "o",
                   "p", "q", "r", "s", "t",
                   "u", "v", "w", "x", "y",
                   "z"]:
        if letter not in sentence:
            return False
    
    return True

print is_pangram("the quick brown fox jumps over the lazy dog")
