'''Pig Latin
Implement a program that translates from English to Pig Latin

Pig Latin is a made-up children's language that's intended to be confusing. It obeys a few simple rules (below), but when it's spoken quickly it's really difficult for non-children (and non-native speakers) to understand.

Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of the word.
Rule 2: If a word begins with a consonant sound, move it to the end of the word, and then add an "ay" sound to the end of the word.
There are a few more rules for edge cases, and there are regional variants too.

See http://en.wikipedia.org/wiki/Pig_latin for more details.
'''

def translate(english):

    vowels = "aeiou"
    suffix = "ay"


    pig_latin = []

    i = 0
    prefix = [] 
    for word in english.split():
        while word[i] not in vowels:
            prefix.append(word[i])
            if word[i] == 'q':    # special case "qu"
                prefix.append(word[i])
                i += 1
            if word[i] == 'x'and word[i+1] not in vowels:
                    break
            if word[i] == 'y'and word[i+1] not in vowels:
                    break
            i += 1
        pig_latin.append(word[i:] + word[0:i] + suffix)
        i = 0

    return " ".join(pig_latin)
