''' run length encoding
Implement run-length encoding and decoding.

Run-length encoding (RLE) is a simple form of data compression, where runs (consecutive data elements) are replaced by just one data value and count.

For example we can represent the original 53 characters with only 13.

1
"WWWWWWWWWWWWBWWWWWWWWWWWWBBBWWWWWWWWWWWWWWWWWWWWWWWWB"  ->  "12WB12W3B24WB"
RLE allows the original data to be perfectly reconstructed from the compressed data, which makes it a lossless data compression.

1
"AABCCCDEEEE"  ->  "2AB3CD4E"  ->  "AABCCCDEEEE"
'''

def encode(uncompressed_string):
    compressed_string = [""]
    # run length works on 2-unit data
    for char in uncompressed_string:
        if char != compressed_string[-1]:
            compressed_string.append(1)
            compressed_string.append(char)
        else:
            compressed_string[-2] += 1
        
    return "".join([unicode(x) for x in compressed_string if x != 1])

def decode(compressed_string):
    count = []
    uncompressed_string = []
    for char in compressed_string:
        if char.isdigit():
            count.append(char)
        else:
            print repr( count)
            count = "".join(count)
            # special case of un-numbered single character
            if count != "":
                count = int(count)
            else: 
                count = 1
            print repr( count)
            uncompressed_string.append(char*count)
            count = []
    return "".join([unicode(x) for x in uncompressed_string])
