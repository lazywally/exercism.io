'''Scrabble Score
Write a program that, given a word, computes the scrabble score for that word.

Letter Values

You'll need these:

1
2
3
4
5
6
7
8
Letter                           Value
A, E, I, O, U, L, N, R, S, T       1
D, G                               2
B, C, M, P                         3
F, H, V, W, Y                      4
K                                  5
J, X                               8
Q, Z                               10
Examples

"cabbage" should be scored as worth 14 points:

3 points for C
1 point for A, twice
3 points for B, twice
2 points for G
1 point for E
And to total:

3 + 2*1 + 2*3 + 2 + 1
= 3 + 2 + 6 + 3
= 5 + 9
= 14
Extensions

You can play a :double or a :triple letter.
You can play a :double or a :triple word.
'''


def score(word):
    value = {}
    
    for c in "AEIOULNRST":
        value[c] = 1
    
    for c in "DG":
        value[c] = 2
    
    for c in "BCMP":
        value[c] = 3
    
    for c in "FHVWY":
        value[c] = 4
    
    for c in "K":
        value[c] = 5
    
    for c in "JX":
        value[c] = 8
    
    for c in "QZ":
        value[c] = 10
    
    word = word.upper()
    score = 0

    for c in word:
        if c not in value.keys():
            return 0
        else:
            score = score + value[c]


    return score
