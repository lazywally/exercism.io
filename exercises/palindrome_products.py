'''Palindrome Products
Detect palindrome products in a given range.

A palindromic number is a number that remains the same when its digits are reversed. For example, 121 is a palindromic number but 112 is not.

Given the definition of a palindromic number, we define a palindrome product to be the product c, such that a * b = c, where c is a palindromic number and a and b are integers (possibly, but not necessarily palindromic numbers).

For example, the palindromic number 9009 can be written as the palindrome product: 91 * 99 = 9009.

It's possible (and indeed common) for a palindrome product to be the product of multiple combinations of numbers. For example, the palindrome product 9 has the factors (1, 9), (3, 3), and (9, 1).

Write a program that given a range of integers, returns the smallest and largest palindromic product within that range, along with all of it's factors.

Example 1

Given the range [1, 9] (both inclusive)...

The smallest product is 1. It's factors are (1, 1). The largest product is 9. It's factors are (1, 9), (3, 3), and (9, 1).

Example 2

Given the range [10, 99] (both inclusive)...

The smallest palindrome product is 121. It's factors are (11, 11). The largest palindrome product is 9009. It's factors are (93, 99) and (99, 91).

Submitting Exercises

Note that, when trying to submit an exercise, make sure the solution is in the exercism/python/<exerciseName> directory.

For example, if you're submitting bob.py for the Bob exercise, the submit command would be something like exercism submit <path_to_exercism_dir>/python/bob/bob.py.

For more detailed information about running tests, code style and linting, please see the help page.

Source

Problem 4 at Project Euler http://projecteuler.net/problem=4
'''

def smallest_palindrome(max_factor, min_factor=1):
    palindromes = []
    for i in range(min_factor, max_factor+1):
        for j in range(min_factor, max_factor+1):
            number = i*j
            if is_palindrome(number):
                palindromes.append(number)

    smallest = min(palindromes)
    factors = find_factors(smallest, min_factor, max_factor)

    return (smallest, factors)

def largest_palindrome(max_factor, min_factor=1):

    palindromes = []

    # range does not include last number
    for i in range(max_factor, min_factor-1, -1):
        for j in range(max_factor, min_factor-1, -1):
            number = i*j
            if is_palindrome(number):
                palindromes.append(number)

    largest = max(palindromes)
    factors = find_factors(largest, min_factor, max_factor)


    return (largest, factors)


def is_palindrome(number):
    number_string = str(number)
    string_list = []

    for c in number_string:
        string_list.append(c)

    reverse_string_list = string_list[::-1]

#    print str(reverse_string_list), str(string_list)
    if reverse_string_list == string_list:
        return True
    else:
        return False


def find_factors(number, min_factor, max_factor):
    factors = []

    for i in range(min_factor, int(number**0.5) +1):
        if number % i == 0:
            if i >= min_factor and number//i <= max_factor:
                if i != number//i:
                    factors.append(i)
                    factors.append(number//i)
                else:
                    factors.append(i)
                return factors


