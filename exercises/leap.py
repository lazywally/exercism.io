''' exercism.io exercise2
determine if leap year.  see unit test.
'''

def is_leap_year(year):
    '''on every year that is evenly divisible by 4 (True)
    except every year that is evenly divisible by 100 (False)
    unless the year is also evenly divisible by 400 (True)
 
    >>> is_leap_year(1996)
    True
    >>> is_leap_year(1997)
    False

    '''

    if year % 400 == 0:
        return True
    elif year % 100 == 0:
        return False
    elif year % 4 == 0:
        return True
    else:
        return False
