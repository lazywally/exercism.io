'''Binary
Convert a binary number, represented as a string (e.g. '101010'), to its decimal equivalent using first principles

Implement binary to decimal conversion. Given a binary input string, your program should produce a decimal output. The program should handle invalid inputs.

Note

Implement the conversion yourself. Do not use something else to perform the conversion for you.
'''

def parse_binary(string):
    decimal = 0
    # 1111 starts at "2**(4-1)" ends at 2**0
    # range does not include "-1"
    maximum = range(len(string)-1, -1, -1)
    for exp, c in zip( maximum, string):
        if c == "0":
            pass
        elif c == "1":
            decimal = decimal + 2**exp
        else:
            raise ValueError("Invalid string")


    return decimal
