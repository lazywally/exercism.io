import string

def word_count(queryString):
    queryString = queryString.decode('utf-8')
    queryString = queryString.lower()

#   stripping punctuation didn't work for the foreign characters
#    for punct in string.punctuation:
#        queryString = queryString.replace(punct, " ")

#   need to resort to checking character by character
    cleanList = [] 
    for char in queryString:
        if char.isalpha() or char.isdecimal():
            cleanList.append(char)
        else:
            cleanList.append(" ")
            
    cleanString = "".join(cleanList)


    words = cleanString.split()
    wordCount = {}
    for word in words:
        #word = word.strip(string.punctuation)
        #if word:
            wordCount[word] = words.count(word)

    return wordCount

print word_count('car : carpet as java : javascript!!&@$%^&')
