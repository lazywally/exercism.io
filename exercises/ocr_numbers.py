'''Ocr Numbers
Given a 3 x 4 grid of pipes, underscores, and spaces, determine which number is represented, or whether it is garbled.

Step One
To begin with, convert a simple binary font to a string containing 0 or 1.

The binary font uses pipes and underscores, four rows high and three columns wide.

1
2
3
4
     _   #
    | |  # zero.
    |_|  #
         # the fourth row is always blank
Is converted to "0"

1
2
3
4
         #
      |  # one.
      |  #
         # (blank fourth row)
Is converted to "1"

If the input is the correct size, but not recognizable, your program should return '?'

If the input is the incorrect size, your program should return an error.

Step Two
Update your program to recognize multi-character binary strings, replacing garbled numbers with ?

Step Three
Update your program to recognize all numbers 0 through 9, both individually and as part of a larger string.

1
2
3
4
 _
 _|
|_

Is converted to "2"

1
2
3
4
      _  _     _  _  _  _  _  _  #
    | _| _||_||_ |_   ||_||_|| | # decimal numbers.
    ||_  _|  | _||_|  ||_| _||_| #
                                 # fourth line is always blank
Is converted to "1234567890"

Step Four
Update your program to handle multiple numbers, one per line. When converting several lines, join the lines with commas.

1
2
3
4
5
6
7
8
9
10
11
12
    _  _
  | _| _|
  ||_  _|

    _  _
|_||_ |_
  | _||_|

 _  _  _
  ||_||_|
  ||_| _|

Is converted to "123,456,789"
'''

grid_key = {
    "0": [" _ ",
          "| |",
          "|_|",
          "   "],
    "1": ["   ",
          "  |",
          "  |",
          "   "],
    "2": [" _ ",
          " _|",
          "|_ ",
          "   "],

    "3": [" _ ",
          " _|",
          " _|",
          "   "],
    "4": ["   ",
          "|_|",
          "  |",
          "   "],
    "5": [" _ ",
          "|_ ",
          " _|",
          "   "],

    "6": [" _ ",
          "|_ ",
          "|_|",
          "   "],

    "7": [" _ ",
          "  |",
          "  |",
          "   "],

    "8": [" _ ",
          "|_|",
          "|_|",
          "   "],

    "9": [" _ ",
          "|_|",
          " _|",
          "   "]
    }

def get_digit(grid):
    for s, g in grid_key.items():
        if (grid == g):
            return s

    return "?"

def grid(num):


    row0 = []
    row1 = []
    row2 = []
    row3 = []

    for c in num:
        if c not in grid_key.keys():
            raise ValueError("Invalid character")

        for s, g in grid_key.items():
            if (c == s):
                row0.append(g[0])
                row1.append(g[1])
                row2.append(g[2])
                row3.append(g[3])
    

    return [ "".join(row0), "".join(row1), "".join(row2), "".join(row3)]



def number(grid):


    full_num = []
    for i in range(0, len(grid[0]), 3):
        try:
            digit = [ grid[0][i:i+3],
                      grid[1][i:i+3],
                      grid[2][i:i+3],
                      grid[3][i:i+3]]
        except:
            raise ValueError("Invalid digit")

        if not is_valid_grid(digit):
            raise ValueError("Invalid digit")

        num = get_digit(digit)

        full_num.append(num)

    return "".join(full_num)

def is_valid_grid(grid):

    if len(grid) != 4:
        return False

    for row in grid:
        if len(row) != 3:
            return False

    return True
