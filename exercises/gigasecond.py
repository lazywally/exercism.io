from datetime import timedelta

def add_gigasecond(present_time):
    '''  add 10^9 seconds to input datetime object.
    return datetime object
    '''

    # cannot add dates, only date+timedelta.  convert gigasecond to timedelta.
    # timedelta limited to 999,999,999 days and 3600*24 seconds, per function 
    # call

    gigasecond = 10**9
    seconds_per_day = 3600*24
    days_per_gigasecond = gigasecond // seconds_per_day
    seconds_left_over = gigasecond % seconds_per_day

    return present_time + timedelta(days_per_gigasecond, seconds_left_over)
