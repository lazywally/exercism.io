def to_rna(dna_sequence):
    '''
    G->C
    C->G
    T->A
    A->U
    '''
    dna_rna = { "G":"C", "C":"G", "T":"A", "A":"U"}
    rna_sequence = ""
    for dna_nucleotide in dna_sequence:
        if dna_nucleotide not in dna_rna.keys():
            return ''
        else:
            rna_nucleotide = dna_rna[dna_nucleotide]
        rna_sequence = rna_sequence + rna_nucleotide
        
    return rna_sequence 
