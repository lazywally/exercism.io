'''Kindergarten Garden
Write a program that, given a diagram, can tell you which plants each child in the kindergarten class is responsible for.

The kindergarten class is learning about growing plants. The teachers thought it would be a good idea to give them actual seeds, plant them in actual dirt, and grow actual plants.

They've chosen to grow grass, clover, radishes, and violets.

To this end, they've put little styrofoam cups along the window sills, and planted one type of plant in each cup, choosing randomly from the available types of seeds.

1
2
3
[window][window][window]
........................ # each dot represents a styrofoam cup
........................
There are 12 children in the class:

Alice, Bob, Charlie, David,
Eve, Fred, Ginny, Harriet,
Ileana, Joseph, Kincaid, and Larry.
Each child gets 4 cups, two on each row. The children are assigned to cups in alphabetical order.

The following diagram represents Alice's plants:

1
2
3
[window][window][window]
VR......................
RG......................
So in the row nearest the window, she has a violet and a radish; in the row behind that, she has a radish and some grass.

Your program will be given the plants from left-to-right starting with the row nearest the windows. From this, it should be able to determine which plants belong to which students.

For example, if it's told that the garden looks like so:

1
2
3
[window][window][window]
VRCGVVRVCGGCCGVRGCVCGCGV
VRCCCGCRRGVCGCRVVCVGCGCV
Then if asked for Alice's plants, it should provide:

Violets, radishes, violets, radishes
While asking for Bob's plants would yield:

Clover, grass, clover, clover
'''

class Garden():

    students = "Alice Bob Charlie David Eve Fred Ginny Harriet Ileana Joseph Kincaid Larry".split()

    plant_names = { "V": "Violets", "C": "Clover", "G": "Grass", "R": "Radishes"}

    def __init__(self, string, students=students):
        students.sort()
        self.students = {}
        i = 0
        for s in students:
           self.students[s] = i
           i+=1

        self.windowsill = string.split()
        pass

    def plants(self, students):
        list_of_plants = []
        students = students.split()
        for s in students:
            list_of_plants.append(self.plant_names[self.windowsill[0][self.students[s]*2]])
            list_of_plants.append(self.plant_names[self.windowsill[0][self.students[s]*2+1]])
            list_of_plants.append(self.plant_names[self.windowsill[1][self.students[s]*2]])
            list_of_plants.append(self.plant_names[self.windowsill[1][self.students[s]*2+1]])

        return list_of_plants


