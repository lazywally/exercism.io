'''Space Age
Write a program that, given an age in seconds, calculates how old someone is in terms of a given planet's solar years.

Given an age in seconds, calculate how old someone would be on:

Earth: orbital period 365.25 Earth days, or 31557600 seconds
Mercury: orbital period 0.2408467 Earth years
Venus: orbital period 0.61519726 Earth years
Mars: orbital period 1.8808158 Earth years
Jupiter: orbital period 11.862615 Earth years
Saturn: orbital period 29.447498 Earth years
Uranus: orbital period 84.016846 Earth years
Neptune: orbital period 164.79132 Earth years
So if you were told someone were 1,000,000,000 seconds old, you should be able to say that they're 31 Earth-years old.

If you're wondering why Pluto didn't make the cut, go watch this youtube video.
'''

class SpaceAge():

    def __init__(self, seconds):
        self.seconds = float(seconds)

    def on_earth(self):
        age = round(self.seconds/31557600, 2)
        return age

    def on_mercury(self):
        age = round( self.seconds/31557600/0.2408467, 2)
        return age

    def on_venus(self):
        age = round( self.seconds/31557600/0.61519726, 2)
        return age

    def on_mars(self):
        age = round( self.seconds/31557600/1.8808158, 2)
        return age

    def on_jupiter(self):
        age = round( self.seconds/31557600/11.862615, 2)
        return age

    def on_saturn(self):
        age = round( self.seconds/31557600/29.447498, 2)
        return age

    def on_uranus(self):
        age = round( self.seconds/31557600/84.016846, 2)
        return age

    def on_neptune(self):
        age = round( self.seconds/31557600/164.79132, 2)
        return age
