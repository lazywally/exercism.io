'''Saddle Points
Write a program that detects saddle points in a matrix.

So say you have a matrix like so:

1
2
3
4
5
    0  1  2
      |---------
      0 | 9  8  7
      1 | 5  3  2     <--- saddle point at (1,0)
      2 | 6  6  7
      It has a saddle point at (1, 0).

      It's called a "saddle point" because it is greater than or equal to every element in its row and the less than or equal to every element in its column.

      A matrix may have zero or more saddle points.

      Your code should be able to provide the (possibly empty) list of all the saddle points for any given matrix.

      Note that you may find other definitions of matrix saddle points online, but the tests for this exercise follow the above unambiguous definition.
'''

def saddle_points(matrix):
    ''' matrix: 2-d list of numbers: [ [1,2],
                                       [3, 4] ]
    return list of point(s)
    '''

    if not matrix:
        return set([])

    rows = matrix
    num_rows = len(matrix)
    num_cols = len(rows[0])

    #make list of columns (from rows)
    cols = [ [None] ] * num_cols 
    for i in range(num_cols):
        cols[i] = [None] * num_rows

    #populate columns
    for i, row in enumerate(rows):
        for j, cell in enumerate(row):
            cols[j][i] = cell


    # check point by point
    saddle_points = []

    for i in range(num_rows):
        for j in range(num_cols):
            if len(matrix[i]) != num_cols:
                raise ValueError("incomplete matrix")
            if matrix[i][j] == min(cols[j]) and matrix[i][j] == max(rows[i]):
                saddle_points.append((i,j))


    return set(saddle_points)


