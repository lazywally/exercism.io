'''Robot Name
Write a program that manages robot factory settings.

When robots come off the factory floor, they have no name.

The first time you boot them up, a random name is generated in the format of two uppercase letters followed by three digits, such as RX837 or BC811.

Every once in a while we need to reset a robot to its factory settings, which means that their name gets wiped. The next time you ask, it will respond with a new random name.

The names must be random: they should not follow a predictable sequence. Random names means a risk of collisions. Your solution should not allow the use of the same name twice when avoidable. In some exercism language tracks there are tests to ensure that the same name is never used twice.
'''

import random
import string

class Robot():

    #global list of robot names to avoid collisions
    names = []

    def __init__(self):
        self.reset()

    def reset(self):
        self.prefix = random.choice(string.uppercase) + random.choice(string.uppercase)
        self.suffix = random.randint(0, 999)
        self.name = self.prefix + str(self.suffix)
        if self.name in self.names:
            self.reset()
        self.names.append(self.name)
        print self.names

    


        
