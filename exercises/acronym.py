'''Acronym
Convert a long phrase to its acronym

Techies love their TLA (Three Letter Acronyms)!

Help generate some jargon by writing a program that converts a long name like Portable Network Graphics to its acronym (PNG).

'''
import string
import re

def abbreviate(long_phrase):
    # add space before Uppercase lowercase.  
    # because must not match consecuative uppercase
    split_phrase = re.sub(r'[A-Z][a-z]+', r' \g<0>', long_phrase)

    # convert to titlecase, and then strip lowercase and strip punctuation
    acronym = split_phrase.title().translate(None, string.lowercase+string.punctuation+" ")
    return acronym
