'''Minesweeper
Add the numbers to a minesweeper board

Minesweeper is a popular game where the user has to find the mines using numeric hints that indicate how many mines are directly adjacent (horizontally, vertically, diagonally) to a square.

In this exercise you have to create some code that counts the number of mines adjacent to a square and transforms boards like this (where * indicates a mine):

1
2
3
4
5
6
+-----+
| * * |
|  *  |
|  *  |
|     |
+-----+
into this:

1
2
3
4
5
6
+-----+
|1*3*1|
|13*31|
| 2*2 |
| 111 |
+-----+
'''

def board(input_list):

    if not is_valid(input_list):
        raise ValueError("input invalid")

    output_list = []
    for r, row in enumerate(input_list):
        output_row = []
        for c, square in enumerate(row):
            if square == " ":
                count = 0
                if row[c+1] == "*":    # right side
                    count += 1
                if row[c-1] == "*":    # left side
                    count += 1
                if input_list[r-1][c] == "*":    # above
                    count += 1
                if input_list[r+1][c] == "*":    # below
                    count += 1
                if input_list[r-1][c-1] == "*":    # above left
                    count += 1
                if input_list[r+1][c-1] == "*":    # below left
                    count += 1
                if input_list[r-1][c+1] == "*":    # above right
                    count += 1
                if input_list[r+1][c+1] == "*":    # below right
                    count += 1
                output_row.append(str(count) if count else " ")
            else:
                output_row.append(square)
        output_row = "".join(output_row)
        output_list.append(output_row)

    return output_list


def is_valid(input_list):

    # check row lengths
    for row in input_list:
        if len(row) != len(input_list[0]):
            return False

    # check characters
    valid_chars = "+-| *"
    board_string = "".join(input_list)

    if board_string.strip(valid_chars) != "":
        return False

    # check upper and bottom border
    if input_list[0] != input_list[-1]:
        return False
    # todo: check side borders

    return True 
