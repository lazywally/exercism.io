'''Matrix
Write a program that, given a string representing a matrix of numbers, can return the rows and columns of that matrix.

So given a string with embedded newlines like:

9 8 7
5 3 2
6 6 7
representing this matrix:

1
2
3
4
5
    0  1  2
      |---------
      0 | 9  8  7
      1 | 5  3  2
      2 | 6  6  7
      your code should be able to spit out:

      A list of the rows, reading each row left-to-right while moving top-to-bottom across the rows,
      A list of the columns, reading each column top-to-bottom while moving from left-to-right.
      The rows for our example matrix:

      9, 8, 7
      5, 3, 2
      6, 6, 7
      And its columns:

      9, 5, 6
      8, 3, 6
      7, 2, 7

      '''

class Matrix():

    def __init__(self, string):

        self.char_rows = string.split('\n')
        self.rows = [None] * len(self.char_rows)

        for i in range(0, len(self.char_rows)):
            self.rows[i] = [ int(j) for j in self.char_rows[i].split()]

        self.columns = [None] * len(self.rows[0])
        for i in range(0, len(self.rows[0])):
            self.columns[i] = [ self.rows[j][i]  for j in range(0, len(self.char_rows))]
