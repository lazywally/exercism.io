'''Write a robot simulator.

A robot factory's test facility needs a program to verify robot movements.

The robots have three possible movements:

turn right
turn left
advance
Robots are placed on a hypothetical infinite grid, facing a particular direction (north, east, south, or west) at a set of {x,y} coordinates, e.g., {3,8}, with coordinates increasing to the north and east.

The robot then receives a number of instructions, at which point the testing facility verifies the robot's new position, and in which direction it is pointing.

The letter-string "RAALAL" means:
Turn right
Advance twice
Turn left
Advance once
Turn left yet again
Say a robot starts at {7, 3} facing north. Then running this stream of instructions should leave it at {9, 4} facing west.
'''

NORTH = 'north'
EAST = 'east'
SOUTH = 'south'
WEST = 'west'

class Robot():

    coordinates = (0, 0)
    bearing = NORTH

    def __init__(self, start_direction = NORTH, startx = 0, starty = 0):
        self.coordinates = (startx, starty)
        self.bearing = start_direction

    def turn_right(self):
        if self.bearing == NORTH:
            self.bearing = EAST
        elif self.bearing == EAST:
            self.bearing = SOUTH
        elif self.bearing == SOUTH:
            self.bearing = WEST
        elif self.bearing == WEST:
            self.bearing = NORTH

    def turn_left(self):
        if self.bearing == NORTH:
            self.bearing = WEST
        elif self.bearing == EAST:
            self.bearing = NORTH
        elif self.bearing == SOUTH:
            self.bearing = EAST
        elif self.bearing == WEST:
            self.bearing = SOUTH


    def advance(self):
        x, y = self.coordinates
        if self.bearing == NORTH:
            y += 1
        elif self.bearing == EAST:
            x += 1
        elif self.bearing == SOUTH:
            y -=1
        elif self.bearing == WEST:
            x -= 1
        self.coordinates = (x, y)
        
    def simulate(self, program_string):
        for action in program_string:
            if action == "A":
                self.advance()
            elif action == "R":
                self.turn_right()
            elif action == "L":
                self.turn_left()






