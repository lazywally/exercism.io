'''Hexadecimal
Convert a hexadecimal number, represented as a string (e.g. "10af8c"), to its decimal equivalent using first principles (i.e. no, you may not use built-in or external libraries to accomplish the conversion).

On the web we use hexadecimal to represent colors, e.g. green: 008000, teal: 008080, navy: 000080).

The program should handle invalid hexadecimal strings.
'''

def hexa(string):
    string = string.lower()
    string = reversed(string)
    hex_key =  "0123456789abcdef"

    hex_multiplier = dict()
    for i, c in enumerate(hex_key):
        hex_multiplier[c] = i

    decimal = 0
    for i, c in enumerate(string):
        if c not in hex_key:
            raise ValueError("not hex value")

        decimal = decimal + hex_multiplier[c] * 16**i

        
    return decimal
