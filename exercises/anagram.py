'''Anagram
Write a program that, given a word and a list of possible anagrams, selects the correct sublist.

Given "listen" and a list of candidates like "enlists" "google" "inlets" "banana" the program should return a list containing "inlets".
'''

def detect_anagrams(word, choices):
    solutions = []
    letters = [ c for c in word.lower()]
    letters.sort()

    for candidate in choices:
        if candidate.lower ()== word.lower():
            pass
        else:
            cand_letters = [ c for c in candidate.lower()]
            cand_letters.sort()
            if letters == cand_letters:
                solutions.append(candidate)

    return solutions
