def distance(strand1, strand2):
    distance = 0
    index = 0
    if len(strand1) != len(strand2):
        raise ValueError

    for nucleotide1 in strand1:
        nucleotide2 = strand2[index]
        if nucleotide1 != nucleotide2:
            distance += 1
        index +=1
    return distance
