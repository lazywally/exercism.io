'''Largest Series Product
Write a program that, when given a string of digits, can calculate the largest product for a contiguous substring of digits of length n.

For example, for the input '1027839564', the largest product for a series of 3 digits is 270 (9 * 5 * 6), and the largest product for a series of 5 digits is 7560 (7 * 8 * 3 * 9 * 5).

Note that these series are only required to occupy adjacent positions in the input; the digits need not be numerically consecutive.

For the input '73167176531330624919225119674426574742355349194934', the largest product for a series of 6 digits is 23520.
'''

def largest_product(string, length):

    if length > len(string) or length < 0:
        raise ValueError("length incompatable with string")
    series = []
    for c in string:
        series.append(int(c))

    product = 1
    largest = 0

    for i in range(0, len(string)-length+1):
        for j in series[i: i+length]:
            product = product * j
        if product > largest:
            largest = product
        product = 1
    return largest
