'''Sum Of Multiples
Write a program that, given a number, can find the sum of all the multiples of particular numbers up to but not including that number.

If we list all the natural numbers up to but not including 20 that are multiples of either 3 or 5, we get 3, 5, 6 and 9, 10, 12, 15, and 18.

The sum of these multiples is 78.

Write a program that, given a number, can find the sum of the multiples of a given set of numbers, up to but not including that number.
'''

def sum_of_multiples(max_number, multiples):
    sum = 0
    for j in range(0,max_number):
         for i in multiples:
            if i !=0 and j % i == 0:    # skip i == 0
                sum += j
                break    # break after the first multiple, ignore the rest

    return sum
