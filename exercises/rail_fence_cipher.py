'''Rail Fence Cipher
Implement encoding and decoding for the rail fence cipher.

The Rail Fence cipher is a form of transposition cipher that gets its name from the way in which it's encoded. It was already used by the ancient Greeks.

In the Rail Fence cipher, the message is written downwards on successive "rails" of an imaginary fence, then moving up when we get to the bottom (like a zig-zag). Finally the message is then read off in rows.

For example, using three "rails" and the message "WE ARE DISCOVERED FLEE AT ONCE", the cipherer writes out:

1
2
3
W . . . E . . . C . . . R . . . L . . . T . . . E
. E . R . D . S . O . E . E . F . E . A . O . C .
. . A . . . I . . . V . . . D . . . E . . . N . .
Then reads off:

1
WECRLTEERDSOEEFEAOCAIVDEN
To decrypt a message you take the zig-zag shape and fill the ciphertext along the rows.

1
2
3
? . . . ? . . . ? . . . ? . . . ? . . . ? . . . ?
. ? . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? .
. . ? . . . ? . . . ? . . . ? . . . ? . . . ? . .
The first row has seven spots that can be filled with "WECRLTE".

1
2
3
W . . . E . . . C . . . R . . . L . . . T . . . E
. ? . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? . ? .
. . ? . . . ? . . . ? . . . ? . . . ? . . . ? . .
Now the 2nd row takes "ERDSOEEFEAOC".

1
2
3
W . . . E . . . C . . . R . . . L . . . T . . . E
. E . R . D . S . O . E . E . F . E . A . O . C .
. . ? . . . ? . . . ? . . . ? . . . ? . . . ? . .
Leaving "AIVDEN" for the last row.

1
2
3
W . . . E . . . C . . . R . . . L . . . T . . . E
. E . R . D . S . O . E . E . F . E . A . O . C .
. . A . . . I . . . V . . . D . . . E . . . N . .
If you now read along the zig-zag shape you can read the original message.
'''

def encode(string, rails):

    fence = make_fence( len(string), rails)
    fence = populate_fence(fence, string)
    print str(fence)

    cipher_text = ""
    for l in fence:
        cipher_text = cipher_text + "".join(l)

    return cipher_text

def decode(string, rails):
    fence = make_fence( len(string), rails)
    fence = populate_fence(fence, string)

    #replace fence
    col = 0
    for rail in fence:
        for i in range(0, len(rail)):
            if rail[i] != "":
                rail[i] = string[col]
                col += 1
        
    print str(fence)
            
    text = ""
    col = 0
    while(col < len(string)):
        # step down fence
        for i in range(0, len(fence)):
            if col >= len(string):
                break
            text = text + fence[i][col]
            col+=1
        # step up fence
        # "-2" : start second to last, "0" second to first
        for i in range(len(fence)-2, 0, -1):
            if col >= len(string):
                break
            text = text + fence[i][col]
            col+=1

    return text
        
        


    

def make_fence(string_length, num_rails):

    fence = [ [""] * string_length for i in range(num_rails)]
    col = 0
    populate_fence(fence, "?" * string_length)

    return fence

def populate_fence(fence, string):
    string_length = len(string)
    col = 0
    while(col < string_length):
        # step down fence
        for i in range(0, len(fence)):
            if col >= string_length:
                break
            fence[i][col]=string[col]
            col+=1
        # step up fence
        # "-2" : start second to last, "0" second to first
        for i in range(len(fence)-2, 0, -1):
            if col >= string_length:
                break
            fence[i][col]=string[col]
            col+=1

    print str(fence)

    return fence
