'''exercism.io exercise3, a Clock class
'''

class Clock:
    ''' see unit tests
    '''
    def __init__(self, hour, minute):
        # roll over minutes into hours, then rollover hours
        # not exactly sure why, but negative time rolls properly
        self.hour = (hour + minute // 60) % 24
        # remaining minutes
        self.minute = minute % 60

    def __str__(self):
        return  "%02d:%02d" % (self.hour, self.minute)

    def __eq__(self, clock2):
        return str(self) == str(clock2)

    def add(self, minutes):
        self.minute = self.minute + minutes
        self.hour = (self.hour + self.minute // 60) % 24
        self.minute = self.minute % 60
        return  "%02d:%02d" % (self.hour, self.minute)

