'''Wordy
Parse and evaluate simple math word problems returning the answer as an integer.

Iteration 1 - Addition

Add two numbers together.

What is 5 plus 13?
Evaluates to 18.

Handle large numbers and negative numbers.

Iteration 2 - Subtraction, Multiplication and Division

Now, perform the other three operations.

What is 7 minus 5?
2

What is 6 multiplied by 4?
24

What is 25 divided by 5?
5

Iteration 3 - Multiple Operations

Handle a set of operations, in sequence.

Since these are verbal word problems, evaluate the expression from left-to-right, ignoring the typical order of operations.

What is 5 plus 13 plus 6?
24

What is 3 plus 2 times 3?
15 (i.e. not 9)

Bonus - Exponentials

If you'd like, handle exponentials.

What is 2 raised to the 5th power?
32
'''

def calculate(question):

    question = question.strip("?")
    question = question.replace("by", "")
    equation = question.split()

    # skip ["What", "is"]
    equation = equation[2:]

    answer = int(equation[0])
    equation = equation[1:]

    for operation, operand in get_operand_and_operator(equation):
        if operation == "plus":
            answer = answer + int(operand)
        elif operation == "minus":
            answer = answer - int(operand)
        elif operation == "multiplied":
            answer = answer * int(operand)
        elif operation == "divided":
            answer = answer / int(operand)
        else:
            raise ValueError("invalid question")

    return answer

def get_operand_and_operator(equation):

    for i in range(0, len(equation), 2):
        operation = equation[i]
        operand = equation[i+1]
    
        yield operation, operand
    

