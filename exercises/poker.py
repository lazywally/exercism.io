'''Poker
Pick the best hand(s) from a list of poker hands.

Write an algorithm to pick the best poker hand(s) from a list.

See wikipedia for an overview of poker hands.
'''

class Hand():
    '''rank is binary flag for easy comparison
    0b10000000 = Straight Flush
    0b01000000 = Four of a kind
    0b00100000 = Full House
    0b00010000 = Flush
    0b00001000 = Straight
    0b00000100 = Three of a kind
    0b00000010 = Two Pair
    0b00000001 = One Pair
    0b00000000 = High Card
    '''

    def __init__(self, hand):
        self.rank = 0b00000000
        self.subrank = 0
        self.hand = hand
        self.values = [card[0] for i, card in enumerate(self.hand)]
        self.suits = [card[1] for i, card in enumerate(self.hand)]
        self.set_rank()

    def set_rank(self):
        if self.is_one_pair():
            self.rank = 0b00000001 
        else: # High Card
            self.rank = 0b00000000

    def is_one_pair(self):

        num_pairs = 0
        self.values.sort()
        for i in range(len(self.values)-1): # last card has no next card to pair
            if (self.values[i] == self.values[i+1]):
                self.subrank = self.values[i]
                num_pairs += 1

        if (num_pairs == 1):
            return True
        
        print num_pairs
        return False


def poker(hands):

    if len(hands) == 1:
        return hands
        
    hand1 = Hand(hands[0])
    hand2 = Hand(hands[1])


    if (hand1.rank > hand2.rank):
        return [hand1.hand]
    elif (hand2.rank > hand1.rank):
        return [hand2.hand]
    elif (hand1.subrank > hand2.subrank):
        return [hand1.hand]
    elif (hand2.subrank > hand1.subrank):
        return [hand2.hand]
    else:
        return "Equal"


