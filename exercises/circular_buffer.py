'''Circular Buffer
A data structure that uses a single, fixed-size buffer as if it were connected end-to-end.

A circular buffer, cyclic buffer or ring buffer is a data structure that uses a single, fixed-size buffer as if it were connected end-to-end.

A circular buffer first starts empty and of some predefined length. For example, this is a 7-element buffer:

1
[ ][ ][ ][ ][ ][ ][ ]
Assume that a 1 is written into the middle of the buffer (exact starting location does not matter in a circular buffer):

1
[ ][ ][ ][1][ ][ ][ ]
Then assume that two more elements are added - 2 & 3 - which get appended after the 1:

1
[ ][ ][ ][1][2][3][ ]
If two elements are then removed from the buffer, the oldest values inside the buffer are removed. The two elements removed, in this case, are 1 & 2, leaving the buffer with just a 3:

1
[ ][ ][ ][ ][ ][3][ ]
If the buffer has 7 elements then it is completely full:

1
[6][7][8][9][3][4][5]
When the buffer is full an error will be raised, alerting the client that further writes are blocked until a slot becomes free.

The client can opt to overwrite the oldest data with a forced write. In this case, two more elements - A & B - are added and they overwrite the 3 & 4:

1
[6][7][8][9][A][B][5]
Finally, if two elements are now removed then what would be returned is not 3 & 4 but 5 & 6 because A & B overwrote the 3 & the 4 yielding the buffer with:

1
[ ][7][8][9][A][B][ ]
'''

class BufferEmptyException(Exception):
    pass

class BufferFullException(Exception):
    pass


class CircularBuffer():

    def __init__(self, size):
        self.size = size
        self.clear()
        self.oldest_pointer = 0
        self.newest_pointer = 0

    def clear(self):
        self.circle_buf = [None] * self.size
        

    def read(self):
        if self.circle_buf == [None] * self.size:
            raise BufferEmptyException
        
        item = self.circle_buf[self.oldest_pointer]
        print str(item )+ str(self.oldest_pointer)
        self.circle_buf[self.oldest_pointer] = None
        self.oldest_pointer = (self.oldest_pointer + 1 ) % self.size
        
        return item

    def write(self, item):
        print str(item) + str(self.newest_pointer)
        if self.circle_buf[self.newest_pointer]:
            raise BufferFullException

        self.circle_buf[self.newest_pointer] = item
        self.newest_pointer = (self.newest_pointer + 1 ) % self.size



    def overwrite(self, item):
        self.circle_buf[self.newest_pointer] = None
        self.write(item)
        #oldest was overwritten into newest.  increment oldest
        self.oldest_pointer = (self.oldest_pointer + 1 ) % self.size
