'''Luhn
Write a program that can take a number and determine whether or not it is valid per the Luhn formula.

The Luhn algorithm is a simple checksum formula used to validate a variety of identification numbers, such as credit card numbers and Canadian Social Insurance Numbers.

The task is to write a function that checks if a given string is valid.

Validating a Number

As an example, here is a valid (but fictitious) Canadian Social Insurance Number.

1
046 454 286
The first step of the Luhn algorithm is to double every second digit, starting from the right. We will be doubling

1
_4_ 4_4 _8_
If doubling the number results in a number greater than 9 then subtract 9 from the product. The results of our doubling:

1
086 858 276
Then sum all of the digits

1
0+8+6+8+5+8+2+7+6 = 50
If the sum is evenly divisible by 10, then the number is valid. This number is valid!

An example of an invalid Canadian SIN where we've changed the final digit

1
046 454 287
Double the second digits, starting from the right

1
086 858 277
Sum the digits

1
0+8+6+8+5+8+2+7+7 = 51
51 is not evenly divisible by 10, so this number is not valid.

An example of an invalid credit card account

1
8273 1232 7352 0569
Double the second digits, starting from the right

1
7253 2262 5312 0539
Sum the digits

1
7+2+5+3+2+2+5+2+5+3+1+2+0+5+3+9 = 57
57 is not evenly divisible by 10, so this number is not valid.

'''

class Luhn():

    def __init__(self, number):
         self.number = number
         self.digits = self.addends()

    def addends(self):
        self.digits = []
        sub_number = self.number

        # bug: unspecified for numbers starting with '0'.  But number is 
        # given as a integer, not a string, so cannot do anything about it.
        while sub_number  > 0:
            self.digits.append(sub_number % 10)    # get last digit
            sub_number = sub_number // 10     # then throw away last digit
            
            if sub_number <= 0:
                pass
            else:
                if 2*(sub_number%10) > 9:
                    # double, then subtract 9
                    self.digits.append(2 * (sub_number % 10) - 9)
                else:
                    # double every second digit
                    self.digits.append(2 * (sub_number % 10))

                sub_number = sub_number // 10     # then throw digit
                

        return self.digits

    def checksum(self):
        
        return sum(self.digits)

    def is_valid(self):
        if self.checksum() % 10 == 0:
            return True
        else:
            return False

    @staticmethod
    def create(number):
        for i in range(0,9):
            temp_luhn = Luhn(number*10+i)
            if temp_luhn.is_valid():
                return temp_luhn.number

