''' bob.py
Bob
Bob is a lackadaisical teenager. In conversation, his responses are very limited.

Bob answers 'Sure.' if you ask him a question.

He answers 'Whoa, chill out!' if you yell at him.

He says 'Fine. Be that way!' if you address him without actually saying anything.

He answers 'Whatever.' to anything else.
'''

def hey(to_bob):
    # nothing
    to_bob = to_bob.strip()
    if to_bob == '':
        return "Fine. Be that way!"

    # Yell at bob
    if to_bob.isupper():
        return "Whoa, chill out!"

    # Question
    # for yelling a question , yelling takes precedence (above)
    if to_bob[-1] == "?":
        return "Sure."


    # anything else
    else:
        return "Whatever."
