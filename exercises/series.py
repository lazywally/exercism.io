'''Series
Write a program that will take a string of digits and give you all the contiguous substrings of length n in that string.

For example, the string "49142" has the following 3-digit series:

491
914
142
And the following 4-digit series:

4914
9142
And if you ask for a 6-digit series from a 5-digit string, you deserve whatever you get.

Note that these series are only required to occupy adjacent positions in the input; the digits need not be numerically consecutive.
'''

def slices(series_string, substring_length):

    if substring_length == 0:
        raise ValueError("substring too short")

    if substring_length > len(series_string):
        raise ValueError("substring too long")

    substrings = []
    for i in range(0, len(series_string) - substring_length +1):
        substring = [int(j) for j in series_string[i: i+substring_length]]
        substrings.append(substring)

    return substrings 
