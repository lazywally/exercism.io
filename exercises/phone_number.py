'''Phone Number
Clean up user-entered phone numbers so that they can be sent SMS messages.

The rules are as follows:

If the phone number is less than 10 digits assume that it is bad number
If the phone number is 10 digits assume that it is good
If the phone number is 11 digits and the first number is 1, trim the 1 and use the last 10 digits
If the phone number is 11 digits and the first number is not 1, then it is a bad number
If the phone number is more than 11 digits assume that it is a bad number
We've provided tests, now make them pass.

Hint: Only make one test pass at a time. Disable the others, then flip each on in turn after you get the current failing one to pass.
'''

class Phone():

    def __init__(self, string):
        self.number = self.clean_number(string)
        self.number = self.validate_number(self.number)

    def clean_number(self, string):
        return string.translate(None, "() -.")

    def validate_number(self, string):
        if len(string) == 10:
            return string

        if len(string) == 11:
            if string[0] == "1":
                return string[1:]
        return "0000000000"

    def area_code(self):
        return self.number[0:3]

    def pretty(self):
        return "(" + self.number[0:3] + ") "+ self.number[3:6] +  "-" +  self.number[6:]

