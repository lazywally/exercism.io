'''Secret Handshake
Given a decimal number, convert it to the appropriate sequence of events for a secret handshake.

There are 10 types of people in the world: Those who understand binary, and those who don't.
You and your fellow cohort of those in the "know" when it comes to binary decide to come up with a secret "handshake".

1
2
3
4
5
6
7
1 = wink
10 = double blink
100 = close your eyes
1000 = jump


10000 = Reverse the order of the operations in the secret handshake.
Here's a couple of examples:

Given the input 3, the function would return the array ["wink", "double blink"] because 3 is 11 in binary.

Given the input 19, the function would return the array ["double blink", "wink"] because 19 is 10011 in binary. Notice that the addition of 16 (10000 in binary) has caused the array to be reversed.
'''

def handshake(number):

    secret = []

    try:
        if isinstance(number, str):
            number = int(number, base=2)
    except:
        return []

    # easier to check largest to smallest (reversed), then reverse into correct order
    if number >= 16:
        reverse_flag = True
        number = number - 16
    else:
        reverse_flag = False
    if number >= 8:
        secret.append("jump")
        number = number - 8
    if number >= 4:
        secret.append("close your eyes")
        number = number - 4
    if number >= 2:
        secret.append("double blink")
        number = number - 2
    if number >= 1:
        secret.append("wink")
        number = number - 1
    
    #already reverse, reverse back to normal
    if not reverse_flag:
        secret.reverse()

    return secret


def code(handshake):

    number = []

    for h in handshake:
        if h == "wink":
            number.append(1)
            continue

        if h == "double blink":
            number.append(2)
            continue

        if h == "close your eyes":
            number.append(4)
            continue

        if h == "jump":
            number.append(8)
            continue

        else:
            number =  [0]

    # if reverse order (reverse-reverse)
    if number[-1] < number[0]:
        number.append(16)

    # strip the '0b' prefix
    number =  sum(number)
    return str( bin(number)[2:])
