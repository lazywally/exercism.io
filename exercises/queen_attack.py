'''Queen Attack
Given the position of two queens on a chess board, indicate whether or not they are positioned so that they can attack each other.

In the game of chess, a queen can attack pieces which are on the same row, column, or diagonal.

A chessboard can be represented by an 8 by 8 array.

So if you're told the white queen is at (2, 3) and the black queen at (5, 6), then you'd know you've got a set-up like so:

1
2
3
4
5
6
7
8
_ _ _ _ _ _ _ _
_ _ _ _ _ _ _ _
_ _ _ W _ _ _ _
_ _ _ _ _ _ _ _
_ _ _ _ _ _ _ _
_ _ _ _ _ _ B _
_ _ _ _ _ _ _ _
_ _ _ _ _ _ _ _
You'd also be able to answer whether the queens can attack each other. In this case, that answer would be yes, they can, because both pieces share a diagonal.
'''

def board(queen1_pos, queen2_pos):

    is_valid_pos(queen1_pos, queen2_pos)

    chessboard = []

    for i in range(0, 8):
        chessboard.append("________")

    row = queen1_pos[0]
    col = queen1_pos[1]

    string = []
    for i in range(0,8):
        if i == col:
            string.append("W")
        else:
            string.append("_")
    chessboard[row] = "".join(string)

    row = queen2_pos[0]
    col = queen2_pos[1]
    string = []
    for i in range(0,8):
        if i == col:
            string.append("B")
        else:
            string.append("_")
    chessboard[row] = "".join(string)


    return chessboard



def can_attack(queen1_pos, queen2_pos):

    is_valid_pos(queen1_pos, queen2_pos)

    attackable = False

    # same row
    if queen1_pos[0] == queen2_pos[0]:
        attackable = True

    # same column
    if queen1_pos[1] == queen2_pos[1]:
        attackable = True

    # diagonal
    for i in range(1, 7):
       if (queen1_pos[0]+i, queen1_pos[1]+i) == queen2_pos:
           attackable = True
       if (queen1_pos[0]-i, queen1_pos[1]+i) == queen2_pos:
           attackable = True
       if (queen1_pos[0]+i, queen1_pos[1]-i) == queen2_pos:
           attackable = True
       if (queen1_pos[0]-i, queen1_pos[1]-i) == queen2_pos:
           attackable = True

    return attackable

def is_valid_pos(queen1_pos, queen2_pos):

    if queen1_pos == queen2_pos:
        raise ValueError("Same position")

    for pos in queen1_pos:
      if pos < 0 or pos > 7:
          raise ValueError("Out of bounds")
          
    for pos in queen2_pos:
      if pos < 0 or pos > 7:
          raise ValueError("Out of bounds")


    return True
