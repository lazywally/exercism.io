'''Sublist
Write a function to determine if a list is a sublist of another list.

Write a function that given two lists determines if the first list is contained within the second list, if the second list is contained within the first list, if both lists are contained within each other or if none of these are true.

Specifically, a list A is a sublist of list B if by dropping 0 or more elements from the front of B and 0 or more elements from the back of B you get a list that's completely equal to A.

Examples:

A = [1, 2, 3], B = [1, 2, 3, 4, 5], A is a sublist of B
A = [3, 4, 5], B = [1, 2, 3, 4, 5], A is a sublist of B
A = [3, 4], B = [1, 2, 3, 4, 5], A is a sublist of B
A = [1, 2, 3], B = [1, 2, 3], A is equal to B
A = [1, 2, 3, 4, 5], B = [2, 3, 4], A is a superlist of B
A = [1, 2, 4], B = [1, 2, 3, 4, 5], A is not a superlist of, sublist of or equal to B
'''

SUBLIST = 'sublist'
SUPERLIST = 'superlist'
EQUAL = 'equal'
UNEQUAL = 'unequal'

def check_lists(list_a, list_b):
    ''' convert list of ints to string, use string built in comparisons
    '''
    string_a = []
    for i in list_a:
        string_a.append(str(i))
    string_a = "".join(string_a)

    string_b = []
    for i in list_b:
        string_b.append(str(i))
    string_b = "".join(string_b)

    if string_a == string_b:
        return EQUAL
    if string_a in string_b:
        return SUBLIST
    if string_b in string_a:
        return SUPERLIST

    return UNEQUAL
