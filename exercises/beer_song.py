''' Beer Song
Write a program which produces the lyrics to that beloved classic, that field-trip favorite: 99 Bottles of Beer on the Wall.

Note that not all verses are identical.

99 bottles of beer on the wall, 99 bottles of beer.
Take one down and pass it around, 98 bottles of beer on the wall.
'''
def verse(num):
    if num == 0:
        return '''No more bottles of beer on the wall, no more bottles of beer.
Go to the store and buy some more, 99 bottles of beer on the wall.
'''
    if num == 1:
        return '''1 bottle of beer on the wall, 1 bottle of beer.
Take it down and pass it around, no more bottles of beer on the wall.
'''

    if num == 2:
        return str(num) + " bottles of beer on the wall, " + str(num) +\
        " bottles of beer.\nTake one down and pass it around, " + str(num-1) + \
        " bottle of beer on the wall.\n"

    else:
        return str(num) + " bottles of beer on the wall, " + str(num) +\
        " bottles of beer.\nTake one down and pass it around, " + str(num-1) + \
        " bottles of beer on the wall.\n"



def song(start, end=0):
    lyrics = []
    for i in range(start, end-1, -1):
        lyrics.append(verse(i))

    return "\n".join(lyrics) + "\n"
