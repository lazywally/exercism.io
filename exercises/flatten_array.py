'''Flatten Array
Write a program that will take a nested list and returns a single list with all values except nil/null

The challenge is to write a function that accepts an arbitrarily-deep nested list-like structure and returns a flattened structure without any nil/null values.

For Example

input: [1,[2,3,null,4],[null],5]

output: [1,2,3,4,5]
'''

def flatten(array, flat=None):
    print flat
    if flat is None:
        flat = []
    if isinstance(array, list) or isinstance(array, tuple):
        print array
        for a in array:
            flat = flatten(a, flat)
        return flat
    if array != None:
        flat.append(array)
    return flat

