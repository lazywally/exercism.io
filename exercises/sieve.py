'''Sieve
Write a program that uses the Sieve of Eratosthenes to find all the primes from 2 up to a given number.

The Sieve of Eratosthenes is a simple, ancient algorithm for finding all prime numbers up to any given limit. It does so by iteratively marking as composite (i.e. not prime) the multiples of each prime, starting with the multiples of 2.

Create your range, starting at two and continuing up to and including the given limit. (i.e. [2, limit])

The algorithm consists of repeating the following over and over:

take the next available unmarked number in your list (it is prime)
mark all the multiples of that number (they are not prime)
Repeat until you have processed each number in your range.

When the algorithm terminates, all the numbers in the list that have not been marked are prime.

The wikipedia article has a useful graphic that explains the algorithm: https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes

Notice that this is a very specific algorithm, and the tests don't check that you've implemented the algorithm, only that you've come up with the correct list of primes.
'''

def sieve(number):
    # add 1 for index
    full_set = [True] * (number+1)

    #special cases
    full_set[0] = False
    full_set[1] = False

    # add 1 for index(es)
    for i in range(2, number+1):
        for j in range (2, number/i +1):
            print i, j
            full_set[i*j] = False
        

    sifted_set = [ i for i in range(0, number+1) if full_set[i] == True]
    
    return sifted_set
