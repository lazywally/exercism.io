'''Bracket Push
Make sure the brackets and braces all match.

Ensure that all the brackets and braces are matched correctly, and nested correctly.
'''

open_brackets = "{[("

def check_brackets(string):
    brackets = []
    for c in string:
        if c in open_brackets:
            brackets.append(c)
        else:
            try:
                if (c == "}" and brackets[-1] == "{" ) or\
                    ( c == "]" and brackets[-1] == "[" ) or\
                    ( c == ")" and brackets[-1] == "(" ):
                    
                    brackets.pop()
                else:
                    return False
            except:
                return False

    return brackets == []
