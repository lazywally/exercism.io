'''Triangle
Determine if a triangle is equilateral, isosceles, or scalene.

The program should raise an error if the triangle cannot exist.

Hint

The triangle inequality theorem states: z <= x + y where x,y, and z are the lengths of the sides of a triangle. In other words, the sum of the lengths of any two sides of a triangle always exceeds or is equal to the length of the third side.

A corollary to the triangle inequality theorem is there are two classes of triangles--degenerate and non-degenerate. If the sum of the lengths of any two sides of a triangle is greater than the length of the third side, that triangle is two dimensional, has area, and belongs to the non-degenerate class. In mathematics, a degenerate case is a limiting case in which an element of a class of objects is qualitatively different from the rest of the class and hence belongs to another, usually simpler, class. The degenerate case of the triangle inequality theorem is when the sum of the lengths of any two sides of a triangle is equal to the length of the third side. A triangle with such qualities is qualitatively different from all the triangles in the non-degenerate class since it is one dimensional, looks like a straight line, and has no area. Such triangles are called degenerate triangles and they belong to the degenerate class.
'''

class Triangle():

    def __init__(self, side_a, side_b, side_c):
        self.side_a = side_a
        self.side_b = side_b
        self.side_c = side_c

        if (not self.is_valid()):
            raise TriangleError

    def is_valid(self):

        if (self.side_a <=0 or self.side_b <=0 or self.side_c <=0):
            return False

        if ( self.side_a + self.side_b <= self.side_c or        
             self.side_b + self.side_c <= self.side_a or        
             self.side_c + self.side_a <= self.side_b):
            return False

        return True


    def kind(self):

        if (self.side_a == self.side_b == self.side_c):
            return "equilateral"
        if (self.side_a == self.side_b != self.side_c or
            self.side_b == self.side_c != self.side_a or
            self.side_c == self.side_a != self.side_b
            ):

            return "isosceles"
        if (self.side_a != self.side_b != self.side_c):
            return "scalene"


class TriangleError(ValueError):

    def __init__(self):
        pass
